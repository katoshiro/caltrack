# caltrack
Calendar tracker that tracks availability (PTO, Public Holidays, partially available and unpaid time off) of employees and contractors.


## Database migrations

### Preparation

To perform migrations, you need `dbmate`. To install it you need the `asdf` package manager:

```sh
apt install curl git sqlite3
git clone https://github.com/asdf-vm/asdf.git $HOME/.asdf --branch v0.13.1

```

For Bash:
```sh
echo '. $HOME/.asdf/asdf.sh' >> $HOME/.bashrc
echo '. $HOME/.asdf/completions/asdf.bash' >> $HOME/.bashrc
```

For oh-my-zsh add this to your plugins in `~/.zshrc`:
```sh
plugins=([other plugins] asdf)
```

Add the `dbmate` tool/plugin:
```sh
asdf plugin-add dbmate
```

Then install `dbmate`:
```sh
asdf install
```

Setup your `.env` file:
```sh
cp .env.example .env
```

### Performing migrations

#### To perform all migrations and bring the database to the latest version:

```sh
dbmate up
```

#### To rollback the last one migration:

```sh
dbmate rollback
# or
dbmate down
```

#### To create a new migration:

```sh
dbmate new <migration_name_with_underscores>
```

and then you edit the migration file in `db/migrations`.